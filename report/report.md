# Exercise 2 - MySQL
# TDT4225

#### Group: 64
#### Students: Trym Grande, Thomas Bjerke

## Introduction
The handout for this exercise consisted of a dataset of trajectories (modified version of Geolife GPS Trajectory dataset), some code to connect to a database using python, and some example code for how to do different queries in MySQL and python. The task was to clean the dataset, create suitable tables in a MySQL database, insert the data, and then execute different queries through python to get data from the database. 

The group consisted of only two members, so communication was easily done in either conference calls or face to face when needed. Git was used throughout the development, but only for version control, as opening issues etc. was not needed in such a small group where both members knew exactly what to work on at all times. Here is a link to the repo: https://gitlab.stud.idi.ntnu.no/trymg/exercise-2-tdt4225 

This exercise was done using a local MySQL database running on linux.

## Results

### Task 1:

Screenshots of top 10 rows for each table:
![](images/1_user.png)
![](images/1_activity.png)
![](images/1_trackpoint.png)

### Task 2:
Query terminal output:
```bash
You are connected to the database: ('ex2',)
-----------------------------------------------

task 2.1
  users    activities    trackpoints
-------  ------------  -------------
    182         16048        9681756
task 2.2
  avg(number_of_activities)    min(number_of_activities)    max(number_of_activities)
---------------------------  ---------------------------  ---------------------------
                    88.1758                            0                         2102
task 2.3
  activities    id
------------  ----
        2102   129
        1793   154
         715    26
         704   164
         691    63
         563   145
         399    42
         364    86
         346     5
         345   141
2.4
  COUNT(*)
----------
        98
task 2.5
user_id    transportation_mode    start_date_time    end_date_time    COUNT(*)
---------  ---------------------  -----------------  ---------------  ----------
task 2.6: (too long runtime, limit is needed)
users in contact: 0
task 2.7
  id    has_labels
----  ------------
   1             0
   2             0
   3             0
   4             0
   5             0
   6             0
   7             0
   8             0
   9             0
  10             0
  12             0
  13             0
  14             0
  15             0
  16             0
  17             0
  18             0
  19             0
  20             0
  21             1
  22             1
  23             0
  24             0
  25             0
  26             0
  27             0
  28             0
  29             0
  30             0
  31             0
  32             0
  33             0
  34             0
  35             0
  36             0
  37             0
  38             0
  39             0
  40             0
  41             0
  42             0
  43             0
  44             0
  45             0
  46             0
  47             0
  48             0
  49             0
  50             0
  51             0
  52             0
  53             1
  54             1
  55             0
  56             0
  57             1
  58             0
  60             1
  61             1
  62             0
  64             0
  65             1
  66             1
  67             0
  68             1
  69             1
  70             1
  71             0
  72             0
  73             0
  74             1
  75             0
  76             1
  77             1
  78             0
  80             0
  82             1
  83             1
  84             0
  85             1
  87             1
  88             1
  89             1
  90             1
  91             0
  92             1
  93             1
  94             0
  95             0
  96             0
  97             1
  98             1
 100             0
 101             1
 102             1
 103             1
 104             0
 105             1
 106             1
 107             1
 108             1
 109             1
 110             0
 111             1
 113             1
 114             0
 115             1
 116             1
 117             1
 118             1
 119             1
 120             0
 121             0
 122             0
 123             0
 124             0
 125             1
 126             1
 127             1
 128             0
 130             1
 131             0
 132             0
 133             0
 134             0
 135             0
 136             0
 137             1
 138             0
 139             1
 140             1
 141             0
 142             1
 143             0
 144             0
 145             1
 146             0
 147             0
 148             1
 149             0
 150             0
 151             0
 152             0
 153             0
 154             1
 155             1
 156             0
 157             0
 158             0
 159             0
 160             0
 161             0
 162             1
 163             0
 165             0
 166             0
 167             0
 168             1
 169             0
 170             0
 171             1
 172             0
 173             0
 174             0
 175             1
 176             1
 177             0
 178             0
 179             0
 180             1
 181             0
 182             0
task 2.8
transportation_mode      number_of_users
---------------------  -----------------
airplane                               1
bike                                  19
boat                                   1
bus                                   13
car                                    8
run                                    1
subway                                 4
taxi                                  10
train                                  2
walk                                  34
task 2.9a
  YEAR(start_date_time)    MONTH(start_date_time)    number_of_activities
-----------------------  ------------------------  ----------------------
                   2008                        11                    1006
task 2.9b - Which user had the most activities 2008-11?
  user_id    number_of_activities
---------  ----------------------
       63                     130
task 2.9b - How many recorder hours does user 63 have in 2008-11?
  SUM(activity_duration_minutes)
--------------------------------
                            2773
task 2.9b - Which user had the second most activities 2008-11?
  user_id    number_of_activities
---------  ----------------------
      129                      75
task 2.9b - Does the user with the most activities 2008-11 have more recorder hours than the user with second most?
  SUM(activity_duration_minutes)
--------------------------------
                            4058
2773 < 4058 - The user with the most activities does not have more recorder hours than the user with the second most activities.

-----------------------------------------------
Connection to 8.0.26 is closed
```

Screenshots for each task:

![](images/2_1.png)
![](images/2_2.png)
![](images/2_3.png)
![](images/2_4.png)
![](images/2_5.png)
![](images/2_6.png)
![](images/2_7_part_1.png)
![](images/2_7_part_2.png)
![](images/2_7_part_3.png)
![](images/2_7_part_4.png)
![](images/2_8.png)
![](images/2_9_a.png)
![](images/2_9_b.png)
![](images/2_10.png)

### Comments
**User Ids** – the user ids start on 1 in the database, as we used AUTO INCREMENT, while in the given dataset, they start on 000. This means that if we want to use user 012 from the given dataset for a query, the id used in the query will be 12+1 = 13. 

**Task 2.6** – See [src/main.py](../src/main.py) for query and script, runtime is too long to show any results.

**Task 2.7** – the assignment text was unclear on whether or not we should only look at users that has labels for transportation_mode. We decided to include all users in the query, so that it returns all users that does not have any activities attached to them with transportation_mode = ‘taxi’.

**Task 2.10** – The query returns no rows, indicating that something is either wrong with the query itself, or something went wrong during the insertion of the data. If some rows had been returned, we would have used Haversine in python to check the distance between each lon, lat-pair, and then added the distances together to find the total distance walked in km in 2008 by user with id = 112. 

## Discussion
We did not do much different than what was suggested. A few small things were:
- Using INT for user.id instead of string
- Excluding trackpoint.date_days, and using substring(date_time) were it was needed.

We wanted to insert multiple rows at the time, and were successfull with the first table. The problem was with the activity table, we had made it way too large due to a bug while merging transportation_mode into the activity table. This caused python to crash while formatting before insertion. We later realized that the insertion would be possible after fixing the bug, but did not have enough time to implement it. This feature was implemented in the "insert_data_universal" function, but is now unused.

Loading the dataset was slightly complicated as we had to switch between different partly unstructured directories.

Task 11 and 12 are unfortunately missing due to lack of time.

## Feedback
The database model shown in the assignment text seemed reasonable to us after reading through the entire assignment, so we decided to implement the tables as described. This worked well, and all the attempted tasks could be solved with that database-setup. 

One of the group members was sick for three days right before the deadline, for which the group got an extended deadline. The group was still not able to do tasks 2.11 and 2.12 in time. If the group had had three members, we would have been able to do those as well. If doing those two tasks will have a big impact on our grade, we ask that we will be able to submit them later, as we have no doubt that we would have been able to solve them with just a bit more time. 

All in all, we have learned a lot from this exercise. Neither of us has ever connected to a database using python before, so learning that was definitely useful. Some of the queries were quite complex, and writing them teached us a lot about SQL-syntax and the logic of relational databases. 
## Feedback
This was a good, educational exercise. The only improvement we would suggest is to make less ambiguous task descriptions, so that it is very clear what the task asks for (e.g. task 2.7).
