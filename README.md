# Exercise 2 in TDT4225

# Installation
```bash
pip install -r handout/requirements.txt
```
See handout/assignment 2 for database setup.
Edit [src/credentials.py](src/credentials.py) with your database credentials.

# Report
See [report/report.md](report/report.md) or [report/report.pdf](report/report.pdf) for report.
